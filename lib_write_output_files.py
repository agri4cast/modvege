def plot(out, gv_b, gr_b, dv_b, dr_b, pgr, gro, abc, sumT, gva, gra, dva, dra, h_b, atr, sea, ftm, env):
    """
    Plot curves of output Vs input

    .. list-table:: Columns description from out_cut.csv
        :widths: 10 15 50 25
        :header-rows: 1

        * - Column
          - Description
          - Unit
          - Array
    	* - 0
          - day
          - The day of the data
          - [day]
        * - 1 
          - Mean biomass
          - (kg DM/ha)
          - gv_b+gr_b+dv_b+dr_b
        * - 2
          - Mean green vegetative biomass
          - (kg DM/ha)
          - gv_b
        * - 3 
          - Mean green reproductive biomass
          - (kg DM/ha)
          - gr_b
        * - 4 
          - Mean dry vegetative biomass
          - (kg DM/ha)
          - dv_b
        * - 5
          - Mean dry reproductive biomass
          - (kg DM/ha)
          - dr_b
        * - 6
          - Harvested Biomass
          - (kg DM/ha)
          - h_b
        * - 7
          - Ingested Biomass
          - (kg DM/ha)
          - i_b
        * - 8
          - Mean GRO biomass
          - (kg DM/ha)
          - gro
        * - 9
          - Mean available biomass for cut
          - (kg DM/ha)
          - abc

    :param out: Input data from the calibration file (out_cut.csv)
    :param gv_b: biomass of green vegetative
    :param gr_b: biomass of green reproductive
    :param dv_b: biomass of dry vegetative
    :param dr_b: biomass of dry reproductive
    :param pgr: potential growth
    :param gro: growth
    :param abc: average biomass available for cut 
    :param sumT: sum of temperature
    :param gva: green vegetative age
    :param gra: green reproductive age
    :param dva: dry vegetative age
    :param dra: dry reproductive age
    :param h_b: harvested biomass
    :param atr: Cumulative allocate to reproductive (REP in Jouven_2006a.pdf, reproductive function)
    :param sea: Seasonal growth
    :param ftm: Fraction of Temperature for mean of 10 days
    :param env: The environmental stress
    :result: plot of multiple graphs providing comparision to calibration data from out_cut.csv
    """
    out_doy = [out[i][0] for i in range(len(out)-1) ]
    out_gvb = [out[i][2] for i in range(len(out)-1) ]
    out_grb = [out[i][3] for i in range(len(out)-1) ]
    out_dvb = [out[i][4] for i in range(len(out)-1) ]
    out_drb = [out[i][5] for i in range(len(out)-1) ]
    out_hb = [out[i][6] for i in range(len(out)-1) ]
    out_ib = [out[i][7] for i in range(len(out)-1) ]
    out_gro = [out[i][8] for i in range(len(out)-1) ]
    out_abc = [out[i][9] for i in range(len(out)-1) ]

    import numpy as np
    import matplotlib.pyplot as plt

    plt.figure(figsize=(15,7))

    plt.subplot(331)
    plt.plot(out_doy,gv_b,'g-',label="gv_b")
    plt.plot(out_doy,out_gvb,'b-',label="out_gvb")
    plt.title('Green Vegetative biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(332)
    plt.plot(out_doy,gr_b,'g-',label="gr_b")
    plt.plot(out_doy,out_grb,'b-',label="out_grb")
    plt.title('Green Reproductive biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(333)
    plt.plot(out_doy,sumT,'g-',label="sumT")
    plt.plot(out_doy,gva,'b-',label="gv_age")
    plt.plot(out_doy,gra,'y-',label="gr_age")
    plt.plot(out_doy,dva,'c-',label="dv_age")
    plt.plot(out_doy,dra,'r-',label="dr_age")
    plt.title('Sum of Temperature (Celsius)')
    plt.legend()
    plt.grid()

    plt.subplot(334)
    plt.plot(out_doy,dv_b,'g-',label="dv_b")
    plt.plot(out_doy,out_dvb,'b-',label="out_dvb")
    plt.title('Dead Vegetative biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(335)
    plt.plot(out_doy,dr_b,'g-',label="dr_b")
    plt.plot(out_doy,out_drb,'b-',label="out_drb")
    plt.title('Dead Reproductive biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(336)
    plt.plot(out_doy,pgr,'m-',label="Pot. Growth")
    plt.plot(out_doy,gro,'g-',label="gro")
    plt.plot(out_doy,out_gro,'b-',label="out_gro")
    plt.title('GRO biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(337)
    plt.plot(out_doy,abc,'g-',label="abc")
    plt.plot(out_doy,out_abc,'b-',label="out_abc")
    plt.title('Mean available biomass for cut (kg DM/ha)')
    plt.legend()
    plt.grid()

    # Harvested Biomass Plot
    plt.subplot(338)
    plt.plot(out_doy,h_b,'g-',label="h_b")
    plt.plot(out_doy,out_hb,'b-',label="out_hb")
    plt.title('Harvested biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(339)
    plt.plot(out_doy,atr,'c-',label="a2r")
    plt.plot(out_doy,sea,'g-',label="Season")
    plt.plot(out_doy,ftm,'r-',label="Temperature")
    plt.plot(out_doy,env,'y-',label="Environmental")
    plt.title('ENV and other Factors')
    plt.legend()
    plt.grid()

    plt.tight_layout()
    plt.savefig('modvege.png')
    plt.show()
