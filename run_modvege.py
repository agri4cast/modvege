#!/usr/bin env python3
import numpy as np

#Import the model function
from modvege import *

# ONLY FOR DEV (CAL/VAL)
from lib_read_output_files import *

# Import ModVege read input files library
from lib_read_input_files import *
# Define the name of the input params file
input_params_csv='params.csv'
# Define the name of the input environment file
input_weather_csv='weather.csv'

# Read Parameter files into array
params = read_params(input_params_csv)

# Read weather file into array
# arr[0][0] = DOY[0] = 1
# arr[0][1] = Temperature[0] = -0.84125
# arr[0][2] = PARi[0] = 2.22092475
# arr[0][3] = PP[0] = 0.119
# arr[0][4] = PET[0] = 0.602689848
# arr[0][5] = ETA[0] = 0.301344 # RS simulated
# arr[0][6] = LAI[0] = 0.864162 # RS simulated
# arr[0][7] = gcut_height[0] = 0.0 [default is 0.05 if cut]
# arr[0][8] = grazing_animal_count[0] = 0 [default is 1 for test ]
# arr[0][9] = grazing_avg_animal_weight[0] = 0 [ default is 400 for cow ]
weather = read_weather(input_weather_csv)

# Modvege does not recognize years, across years should be split in two processings.
startdoy = 1
enddoy = 365

# Initialize the run and return arrays
gv_b, dv_b, gr_b, dr_b, h_b, i_b, gro, abc, sumT, gva, gra, dva, dra, sea, ftm, env, pgr, atr = modvege(params, weather, startdoy, enddoy)
    

# output in table and .csv file
f = open("modvege_out.csv", "w")
f.write("time, harvestable_biomass, harvested_yield\n")
for t in range(len(h_b)):
    f.write("%d,%.2f,%.2f\n" % (t+startdoy, abc[t], h_b[t]))
f.close()


# ONLY FOR DEV
#out = read_out(out_csv)
    
#from lib_write_output_files import *
#out = plot(out, gv_b, gr_b, dv_b, dr_b, pgr, gro, abc, sumT, gva, gra, dva, dra, h_b, atr, sea, ftm, env)

