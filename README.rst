MODVEGE
=======

MODVEGE in Python bypassed by RS data in many places @EUPL

This code inherits from a Java version kindly provided by Raphael Martin (INRAE, Clermont-Ferrand, FR)


.. inclusion-marker-do-not-remove

How to use Modvege software
===========================

You need to install:
 - Python (> 3.10) 
 - Numerical Python *(pip install numpy)*

How to use Modvege as a function
================================

The function `modvege()` is found in `modvege.py`.

You can feed numpy arrays directly to the function, see how it is done in `run_modvege.py`

.. literalinclude:: ../../run_modvege.py
  :language: Python

:Output:

A set of graphs will be written to a PNG (`modvege.png`) from the run.

.. image:: ../../modvege.png
  :width: 800


How to contribute
=================

Please contact the Agri4Cast_ team

.. _Agri4cast: eu-agri4cast-info@ec.europa.eu

JRC, Ispra, Italy

Other related pasture models
============================
- PaSim [INRAE, Clermont]
- Basgra [https://code.europa.eu/Agri4Cast/basgra]
- LINGRA [https://code.europa.eu/Agri4Cast/lingra]

